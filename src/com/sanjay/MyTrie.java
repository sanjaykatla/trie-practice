package com.sanjay;

import java.util.ArrayList;
import java.util.List;

public class MyTrie {

    private TrieNode root = new TrieNode('\0');

    public static void main(String[] args) {

        MyTrie trie = new MyTrie();
        trie.add("apple");
        trie.add("banana");
        trie.add("apihome");

        System.out.println(trie.isStringPresent("apple"));
        System.out.println(trie.isStringPresent("app"));
        System.out.println(trie.isStringPresent("banana"));
        System.out.println(trie.isStringPresent("apihome"));
        System.out.println(trie.isStringPresent("apihom"));

        System.out.println("\nSearch ... \n");
        for(String s: trie.search("ap")){
            System.out.println(s);
        }
    }

    public void add(String s){

        TrieNode temp = root;

        for(int i=0; i < s.length(); i++){
            char c = s.charAt(i);

            if(temp.children[c -'a'] == null){
                temp.children[c-'a'] = new TrieNode(c);
            }
            temp = temp.children[c-'a'];
        }
        temp.isTerminating = true;
    }

    public List<String> search(String prefix) {

        List<String> result = new ArrayList<>();

        TrieNode temp = root;

        for(int i=0; i < prefix.length(); i++){
            char c = prefix.charAt(i);

            if(temp.children[c -'a'] == null){
                return result;
            }
            temp = temp.children[c-'a'];
        }

        dfs(temp, prefix, "", result);

        return result;
    }

    private void dfs(TrieNode node, String prefix, String output, List<String> result) {

        if(node == null){
            return;
        }

        if(node.isTerminating){
            result.add(prefix + output);
        }

        for(TrieNode trieNode: node.children){
            if(trieNode != null){
                dfs(trieNode, prefix, output+trieNode.val, result);
            }
        }
    }

    public boolean isStringPresent(String s){

        TrieNode temp = root;

        for(int i=0; i < s.length(); i++){
            char c = s.charAt(i);

            if(temp.children[c -'a'] == null){
                return false;
            }
            temp = temp.children[c-'a'];
        }
        return temp.isTerminating;
    }
}
