package com.sanjay;

public class TrieNode {

    char val;
    boolean isTerminating;
    TrieNode[] children = new TrieNode[26];

    public TrieNode(char val){
        this.val = val;
    }
}
